from operator import xor

import Aes
from Aes import Aes_data


class Desencriptar:
    __nb = 4
    vi = 0

    def __init__(self, block):
        self.block = block
        #self.key = key

    def inv_subbyte(self):
        for i in range(self.__nb):
            for j in range(self.__nb):
                self.block[i][j] = Aes_data.is_box[self.block[i][j]]
        return self.block

    def inv_shift_rows(self):
        # cambio en la fila 1
        self.block[1][0], self.block[1][1], self.block[1][2], self.block[1][3] = self.block[1][3], self.block[1][
            0], self.block[1][1], self.block[1][2]
        # cambio en la fila 2
        self.block[2][0], self.block[2][1], self.block[2][2], self.block[2][3] = self.block[2][2], self.block[2][
            3], self.block[2][0], self.block[2][1]
        # cambio de la fila 3
        self.block[3][0], self.block[3][1], self.block[3][2], self.block[3][3] = self.block[3][1], self.block[3][
            2], self.block[3][3], self.block[3][0]
        return self.block
        pass

    def inv_mix_columns(self):
        n_mod = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        for i in range(self.__nb):
            n_mod[0][i] = Aes.Aes_data.gmod14[self.block[0][i]] ^ Aes.Aes_data.gmod11[self.block[1][i]] ^ \
                          Aes.Aes_data.gmod13[self.block[2][i]] ^ Aes.Aes_data.gmod9[self.block[3][i]]

            n_mod[1][i] = Aes.Aes_data.gmod9[self.block[0][i]] ^ Aes.Aes_data.gmod14[self.block[1][i]] ^ \
                          Aes.Aes_data.gmod11[self.block[2][i]] ^ Aes.Aes_data.gmod13[self.block[3][i]]

            n_mod[2][i] = Aes.Aes_data.gmod13[self.block[0][i]] ^ Aes.Aes_data.gmod9[self.block[1][i]] ^ \
                          Aes.Aes_data.gmod14[self.block[2][i]] ^ Aes.Aes_data.gmod11[self.block[3][i]]

            n_mod[3][i] = Aes.Aes_data.gmod11[self.block[0][i]] ^ Aes.Aes_data.gmod13[self.block[1][i]] ^ \
                          Aes.Aes_data.gmod9[self.block[2][i]] ^ Aes.Aes_data.gmod14[self.block[3][i]]
        self.block = n_mod
        return self.block

    def inv_add_round_key(self, key):
        # XOR
        for i in range(self.__nb):
            for j in range(self.__nb):
                self.block[i][j] = xor(key[i][j], self.block[i][j])
        return self.block

    def get_block(self):
        return self.block
