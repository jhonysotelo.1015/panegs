from operator import xor
import Aes.Aes_data as Aes_data


class Encriptar:

    __nb = 4
    vi = 0

    def __init__(self, block, key):
        self.block = block
        self.__key = key

    def expansion_key(self):
        # rotWord
        rotword = [0, 0, 0, 0]
        for i in range(1, self.__nb):
            rotword[i-1] = self.__key[i][3]
        rotword[3] = self.__key[0][3]
        # subByte
        for i in range(self.__nb):
            rotword[i] = Aes_data.s_box[rotword[i]]
        # Xor [i-3] && Xor Rcon
        for i in range(self.__nb):
            rotword[i] = xor(rotword[i], self.__key[i][0])
            rotword[i] = xor(rotword[i], Aes_data.Rcon[self.vi][i])
        # new key i
        temp = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        for i in range(self.__nb):
            for j in range(self.__nb):
                if i == 0:
                    self.__key[j][i] = rotword[j]
                    temp[j][i] = rotword[j]
                else:
                    temp[j][i] = xor(self.__key[j][i], self.__key[j][i - 1])
                    self.__key[j][i] = xor(self.__key[j][i], self.__key[j][i-1])

        self.vi += 1
        return temp

    def add_round_key(self):
        # XOR
        for i in range(self.__nb):
            for j in range(self.__nb):
                self.block[i][j] = xor(self.__key[i][j], self.block[i][j])
        return self.block

    def sub_bytes(self):
        for i in range(self.__nb):
            for j in range(self.__nb):
                self.block[i][j] = Aes_data.s_box[self.block[i][j]]
        return self.block

    def shift_rows(self):
        # cambio en la fila 1
        self.block[1][0], self.block[1][1], self.block[1][2], self.block[1][3] = self.block[1][1], self.block[1][
            2], self.block[1][3], self.block[1][0]
        # cambio en la fila 2
        self.block[2][0], self.block[2][1], self.block[2][2], self.block[2][3] = self.block[2][2], self.block[2][
            3], self.block[2][0], self.block[2][1]
        # cambio de la fila 3
        self.block[3][0], self.block[3][1], self.block[3][2], self.block[3][3] = self.block[3][3], self.block[3][
            0], self.block[3][1], self.block[3][2]

        return self.block

    def mix_columns(self):

        m_aux = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        # column 0
        for i in range(self.__nb):
            m_aux[i][0] = self.gmod(self.block[0][0], Aes_data.m_gal[i][0]) ^ \
                          self.gmod(self.block[1][0], Aes_data.m_gal[i][1]) ^ \
                          self.gmod(self.block[2][0], Aes_data.m_gal[i][2]) ^ \
                          self.gmod(self.block[3][0], Aes_data.m_gal[i][3])
        # column 1
        for i in range(self.__nb):
            m_aux[i][1] = self.gmod(self.block[0][1], Aes_data.m_gal[i][0]) ^ \
                          self.gmod(self.block[1][1], Aes_data.m_gal[i][1]) ^ \
                          self.gmod(self.block[2][1], Aes_data.m_gal[i][2]) ^ \
                          self.gmod(self.block[3][1], Aes_data.m_gal[i][3])
        # column 2
        for i in range(self.__nb):
            m_aux[i][2] = self.gmod(self.block[0][2], Aes_data.m_gal[i][0]) ^ \
                          self.gmod(self.block[1][2], Aes_data.m_gal[i][1]) ^ \
                          self.gmod(self.block[2][2], Aes_data.m_gal[i][2]) ^ \
                          self.gmod(self.block[3][2], Aes_data.m_gal[i][3])
        # column 3
        for i in range(self.__nb):
            m_aux[i][3] = self.gmod(self.block[0][3], Aes_data.m_gal[i][0]) ^ \
                          self.gmod(self.block[1][3], Aes_data.m_gal[i][1]) ^ \
                          self.gmod(self.block[2][3], Aes_data.m_gal[i][2]) ^ \
                          self.gmod(self.block[3][3], Aes_data.m_gal[i][3])

        self.block = m_aux
        return self.block

    def gmod(self, matriz_value, galois_value):
        if galois_value == 1:
            return matriz_value
        tmp = (matriz_value << 1) & 0xff
        if galois_value == 2:
            return tmp if matriz_value < 128 else tmp ^ 0x1b
        if galois_value == 3:
            return self.gmod(matriz_value, 2) ^ matriz_value

    def get_block(self):
        return self.block
