import marshal

from Aes.AesEncrypter import Encriptar
from Aes.AesDesencrypter import Desencriptar

keys = []


def imp_matriz(matriz):
    print()
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            print(hex(matriz[i][j]), end=" ")
        print("")


def add_key(key):
    keys.append(key)


def prepare_matrix(value):
    m_value = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    letter = 0
    for i in range(4):
        for j in range(4):
            m_value[j][i] = ord(value[letter])
            letter += 1

    return m_value


def prepare_text(matriz):
    cadena = ""
    for i in range(4):
        for j in range(4):
            cadena += chr(matriz[j][i])

    return cadena


def encriptar_info(valor, key, guardar):
    key_0 = prepare_matrix(key)
    value = prepare_matrix(valor)
    # Comienzo del algoritmo
    round_ = 0
    aes_cypher = Encriptar(value, key_0)

    aes_cypher.add_round_key()
    while round_ != 9:
        aes_cypher.sub_bytes()
        aes_cypher.shift_rows()
        aes_cypher.mix_columns()
        keyi = aes_cypher.expansion_key()
        add_key(keyi)
        aes_cypher.add_round_key()
        round_ += 1
    aes_cypher.sub_bytes()
    aes_cypher.shift_rows()
    keyi = aes_cypher.expansion_key()
    add_key(keyi)
    aes_cypher.add_round_key()
    #imp_matriz(aes_cypher.get_block())
    if guardar:
        with open("key", "bw") as dato:
            marshal.dump(keys, dato)
            dato.close()

    return prepare_text(aes_cypher.get_block())


def desencriptar_info(matriz, key, key_i):

    key_0 = prepare_matrix(key)
    prepare = prepare_matrix(matriz)
    # Comienzo del algoritmo
    round_ = 0
    aes_cypher = Desencriptar(prepare)

    aes_cypher.inv_add_round_key(key_i[9])
    aes_cypher.inv_shift_rows()
    aes_cypher.inv_subbyte()
    while round_ != 9:
        aes_cypher.inv_add_round_key(key_i[8-round_])
        aes_cypher.inv_mix_columns()
        aes_cypher.inv_shift_rows()
        aes_cypher.inv_subbyte()
        round_ += 1
    aes_cypher.inv_add_round_key(key_0)
    return prepare_text(aes_cypher.get_block())