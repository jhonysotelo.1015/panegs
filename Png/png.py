import marshal

import Aes.metodos


def is_png(url):
    with open(url, "rb") as image:
        f = image.read()
        b = bytearray(f)
        for i in range(len(b)):
            if b[i] == 0x89 and b[i + 1] == 0x50 and b[i + 2] == 0x4e and b[i + 3] == 0x47 and b[i + 4] == 0x0d \
                    and b[i + 5] == 0x0a and b[i + 6] == 0x1a and b[i + 7] == 0x0a:
                return True
            else:
                return False


def reemplazar(cifrado, url, posicion):
    with open(url, "rb") as image:
        f = image.read()
        b = bytearray(f)
        for i in range(16):
            b[posicion+i] = ord(cifrado[i])
    with open(url, "wb") as final:
        final.write(b)
        final.close()


def transformar(url, key):
    cadena = ""
    guardar = True
    with open(url, "rb") as image:
        f = image.read()
        b = bytearray(f)
        for i in range(len(b)):
            if b[i] == 0x49 and b[i+1] == 0x44 and b[i+2] == 0x41 and b[i+3] == 0x54:
                for j in range(i+4, len(b)):
                    if b[j] == 0x49 and b[j+1] == 0x45 and b[j+2] == 0x4e and b[j+3] == 0x44:
                        break
                    else:
                        # Dentro del IDAT
                        if len(cadena) == 16:
                            if guardar:
                                cifrado = Aes.metodos.encriptar_info(cadena, key, guardar)
                                guardar = False
                            else:
                                cifrado = Aes.metodos.encriptar_info(cadena, key, False)
                            print("bloque: ", j, "de:", len(b))
                            reemplazar(cifrado, url, j-16)
                            cadena = ""
                        else:
                            cadena += chr(b[j])
                break


def revertir(url_in, keys, key_in):
    cadena_in = ""
    fileIn = open(keys, "br")
    dataLoad = marshal.load(fileIn)
    fileIn.close()
    with open(url_in, "rb") as image_in:
        f_in = image_in.read()
        b_in = bytearray(f_in)
        for i in range(len(b_in)):
            if b_in[i] == 0x49 and b_in[i + 1] == 0x44 and b_in[i + 2] == 0x41 and b_in[i + 3] == 0x54:
                for j in range(i + 4, len(b_in)):
                    if b_in[j] == 0x49 and b_in[j + 1] == 0x45 and b_in[j + 2] == 0x4e and b_in[j + 3] == 0x44:
                        break
                    else:
                        # Dentro del IDAT
                        if len(cadena_in) == 16:
                            descifrado = Aes.metodos.desencriptar_info(cadena_in, key_in, dataLoad)
                            reemplazar(descifrado, url_in, j - 16)
                            print("bloque: ", j, "de:", len(b_in))
                            cadena_in = ""
                        else:
                            cadena_in += chr(b_in[j])
                break

        pass


if __name__ == '__main__':
    # with open("imaF.png", "wb") as final:
    #   final.write(b)
    pass
