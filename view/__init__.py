from tkinter import Tk, PhotoImage, ttk, filedialog
from Png import png


class GUI:
    def __init__(self):
        self.raiz = Tk()
        self.raiz.geometry("600x500")
        self.raiz.configure(background="#082E7A")
        self.raiz.resizable(False, False)
        self.raiz.title("Panegs")
        self.encriptar()

        imagen = PhotoImage(file="menu.png")
        self.etiqueta_menu = ttk.Label(self.raiz, text="Menu", image=imagen, compound="left")
        self.etiqueta_menu.configure(font=("Arial", 20), background="#082E7A", foreground="#ffffff")
        self.etiqueta_menu.place(x=10, y=10)

        img_boton = PhotoImage(file="encrip.png")
        self.btn_encriptar = ttk.Button(self.raiz, text="Encriptar", command=self.encriptar, image=img_boton,
                                        compound="left", width=12)
        self.btn_encriptar.place(x=10, y=100)

        img_boton1 = PhotoImage(file="desencrip.png")
        self.btn_desencriptar = ttk.Button(self.raiz, text="Desencriptar", command=self.desencriptar,
                                           image=img_boton1, compound="left", width=12)
        self.btn_desencriptar.place(x=10, y=200)

        self.btn_salir = ttk.Button(self.raiz, text="Salir", command=quit, width=12)
        self.btn_salir.place(x=10, y=400)

        self.raiz.mainloop()

    def encriptar(self):

        def file_chooser():
            global url
            url = filedialog.askopenfilename(title='Choose a Image', initialdir='/home/jonnathan/Descargas',
                                             filetypes=(('Imagen png', '*.png'),
                                                        ))

        child = ttk.Frame(self.raiz, width=400, height=400, padding=10)

        lbl_clave = ttk.Label(child, text="Ingrese Clave de 16 Caracteres")
        lbl_clave.place(x=20, y=180)

        password = ttk.Entry(child)
        password.place(x=20, y=210, width=120)

        lbl_archivo = ttk.Label(child, text="Añadir archivo")
        lbl_archivo.place(x=300, y=180)

        btn_add_image = ttk.Button(child, text="add Image", command=file_chooser)
        btn_add_image.place(x=300, y=210)

        lbl_encriptar = ttk.Label(child, text="Encriptar", font=("Arial", 24))
        lbl_encriptar.place(x=20, y=20)

        btn_add_image = ttk.Button(child, text="Cifrar", command=lambda: self.operar_encriptado(url, password.get()))
        btn_add_image.place(x=180, y=340)

        child.place(x=180, y=80)

    def desencriptar(self):
        def file_chooser():
            global url0
            url0 = filedialog.askopenfilename(title='Choose a Image', initialdir='/home/jonnathan/Descargas',
                                              filetypes=(('Imagen png', '*.png'),
                                                         ))

        def file_chooser_key():
            global key_arc
            key_arc = filedialog.askopenfilename(title='Choose a Image', initialdir='/home/jonnathan/PycharmProyects',
                                                 filetypes=(('key', '*'),
                                                            ))

        child = ttk.Frame(self.raiz, width=400, height=400, padding=10)

        lbl_clave = ttk.Label(child, text="Cargar Llave")
        lbl_clave.place(x=40, y=180)

        password = ttk.Button(child, text="cargar", command=file_chooser_key)
        password.place(x=40, y=210, width=120)

        lbl_password = ttk.Label(child, text="Clave de Desencriptado")
        lbl_password.place(x=40, y=290)

        password_input = ttk.Entry(child)
        password_input.place(x=40, y=310, width=120)

        lbl_archivo = ttk.Label(child, text="Añadir archivo")
        lbl_archivo.place(x=250, y=180)

        btn_add_image = ttk.Button(child, text="add Image", command=file_chooser)
        btn_add_image.place(x=250, y=210)

        lbl_encriptar = ttk.Label(child, text="DesEncriptar", font=("Arial", 24))
        lbl_encriptar.place(x=20, y=20)

        btn_add_image = ttk.Button(child, text="Cifrar",
                                   command=lambda: self.operar_desencriptado(url0, key_arc, password_input.get()))
        btn_add_image.place(x=250, y=300)

        child.place(x=180, y=80)

    def operar_encriptado(self, ruta, key):
        if ruta is None:
            print("Ruta no Encontrada")
            pass
        else:
            if png.is_png(ruta):
                png.transformar(ruta, key)

    def operar_desencriptado(self, ruta, keys, key):
        if ruta is None:
            print("Ruta no Encontrada")
            pass
        else:
            if png.is_png(ruta):
                print(ruta, "----------", keys, "-----------", key)
                png.revertir(ruta, keys, key)
        pass


if __name__ == '__main__':
    interfaz = GUI()
